define(['jquery'], function($) {
  'use strict';
  /**
   * Adds Play and Pause functionality to a Slick Carousel component
   * @param slickContainer {jQuery} Required jQuery container that the slick
   *   component was made from
   * @param options {object}
   *  - attachTo {jQuery} An optional jQuery object to inject the play and
   *      pause controls
   *  - playBtn {html string} HTML to be used as the play button
   *  - pauseBtn {html string} HTML to be used as the pause button
   */
  var SlickPlayPauseControls = function(slickContainer, options) {
    var _ = this;

    // quit on errors
    if (_.error = _.verify(slickContainer)) {
      return console.log(_.error);
    }

    // assign variables
    _.slickContainer = slickContainer;
    _.slickInstance = slickContainer.get(0).slick;
    _.settings = $.extend(true, {}, _._defaults, options);

    // set scope
    _.play  = $.proxy(_.play, _);
    _.pause = $.proxy(_.pause, _);
    _.onBreakpointChange = $.proxy(_.onBreakpointChange, _);

    // create
    _.$playBtn = _.addPlayBtn();
    _.$pauseBtn = _.addPauseBtn();

    // set breakpoint listener
    _.slickContainer.on('breakpoint', _.onBreakpointChange);

    // set current state
    _.setCurrentBreakpointState();
  };

  SlickPlayPauseControls.prototype = {
    _defaults: {
      slickInstance: null,
      slickContainer: null,
      attachTo: null,
      playBtn: '<button class="slick-play slick-btn" type="button" aria-label="Play">Play</button>',
      pauseBtn: '<button class="slick-pause slick-btn" type="button" aria-label="Pause">Pause</button>'
    },
    verify: function(slickContainer) {
      var error;
      if (!slickContainer || !slickContainer.length) {
        error = "SlickPlayPauseControls: The slickContainer was't found or passed";
      }
      else if (!slickContainer.get(0).slick) {
        error = "SlickPlayPauseControls: The slickInstance wasn't found";
      }
      return error;
    },
    addPlayBtn: function() {
      return $(this.settings.playBtn);
    },
    addPauseBtn: function() {
      return $(this.settings.pauseBtn);
    },
    addControls: function() {
      // Allows the controls to be attached to a passed jQuery object or default
      // to the passed in container
      if (!this.settings.attachTo) {
        this.settings.attachTo = this.slickContainer;
      }

      // Add play and pause the controls and finally add the controls container
      this.settings.attachTo.prepend(this.$playBtn, this.$pauseBtn);

      // Add click handlers to each button to control autoplay functionality
      this.$playBtn.on('click', this.play);
      this.$pauseBtn.on('click', this.pause);

      // set the current state of these buttons we just added
      this.setCurrentState();
    },
    removeControls: function() {
      // remove the buttons
      this.$playBtn.off('click', this.play).remove();
      this.$pauseBtn.off('click', this.pause).remove();
    },
    play: function(event) {
      event.preventDefault();
      if (this.slickInstance.paused) {
        this.slickContainer.slick('slickPlay');
      }
      this.setCurrentState();
    },
    pause: function(event) {
      event.preventDefault();
      if (!this.slickInstance.paused) {
        this.slickContainer.slick('slickPause');
      }
      this.setCurrentState();
    },
    setPlayState: function() {
      this.$playBtn.attr('aria-hidden', 'false');
      this.$pauseBtn.attr('aria-hidden', 'true');
    },
    setPauseState: function() {
      this.$playBtn.attr('aria-hidden', 'true');
      this.$pauseBtn.attr('aria-hidden', 'false');
    },
    setCurrentBreakpointState: function() {
      var activeBreakpoint = this.slickInstance.activeBreakpoint;
      // capture the currently active autoplay variable
      var autoplay = activeBreakpoint
        ? this.slickInstance.breakpointSettings[activeBreakpoint].autoplay
        : this.slickInstance.options.autoplay;

      // manage our controls accordingly
      autoplay
        ? this.addControls()
        : this.removeControls();
    },
    setCurrentState: function() {
      this.slickInstance.paused
        ? this.setPlayState()
        : this.setPauseState()
    },
    // Integrates with the slick carousel breakpoint functionality
    onBreakpointChange: function(event, slick, breakpoint) {
      this.setCurrentBreakpointState();
    }
  };

  return SlickPlayPauseControls;
});
